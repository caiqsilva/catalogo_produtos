import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, LoadingController } from '@ionic/angular';
import { UrlService } from '../../provider/url.service';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ServiceUserService } from '../../provider/service-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    
  email: string;
  senha: string;

  constructor(public alert: AlertController, public urlService: UrlService, public http: Http, public nav: NavController, public loading: LoadingController, public serviceUser: ServiceUserService) { 

    this.email = "glidersi@gmail.com";
    this.senha = "123";
    

    if(localStorage.getItem('deslogado')=="sim"){
        localStorage.setItem('deslogado',"não");
        location.reload();
    }

    if(localStorage.getItem('user_logado') != null){
        console.log('autenticação');
        this.autenticar();
        this.nav.navigateForward('home');
    }
    
  }

  ngOnInit() {
  }

  


  async logar() {

   
      console.log("vamos autenticar");
    if (this.email == undefined || this.senha == undefined) {

      this.urlService.alertas('Atenção', 'Preencha todos os campos!');

    } else {


      this.urlService.exibirLoading();

      this.http.get(this.urlService.getUrl() + "login.php?email=" + this.email + "&senha=" + this.senha).pipe(map(res => res.json()))
        .subscribe(

          data => {


            if (data.msg.logado == "sim") {
              if(data.dados.status == "Ativo"){
                this.serviceUser.setUserId(data.dados.idusuario);
                this.serviceUser.setUserNome(data.dados.nome);
                this.serviceUser.setUserNivel(data.dados.nivel);
                this.serviceUser.setUserFoto(data.dados.foto);
                localStorage.setItem('idUser', data.dados.idusuario);
                localStorage.setItem('NameUser', data.dados.nome);
                localStorage.setItem('nivelUser', data.dados.nivel);
                localStorage.setItem('setUserFoto', data.dados.foto);
               
                this.urlService.fecharLoding();
                localStorage.setItem('user_logado', data);
                this.nav.navigateBack('home');
              }else{
                this.urlService.fecharLoding();
                this.urlService.alertas('Atenção','Usuário Bloqueado!');

              }

            } else {
              this.urlService.fecharLoding();
              this.urlService.alertas('Atenção','Usuário ou senha incorretos!');

            }

          }, error =>{
            this.urlService.fecharLoding();
            this.urlService.alertas('Atenção', 'Algo deu erro, verifique sua conexão com a internet');
          }

        );


    }
  

  }

  autenticar(){
    this.serviceUser.setUserId(localStorage.getItem('idUser'));
    this.serviceUser.setUserNome(localStorage.getItem('NameUser'));
    this.serviceUser.setUserNivel(localStorage.getItem('nivelUser'));
    this.serviceUser.setUserFoto(localStorage.getItem('setUserFoto'));
  }



}
