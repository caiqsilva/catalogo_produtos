import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import { UrlService } from '../../provider/url.service';
import { NavController, NavParams } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ServiceUserService } from '../../provider/service-user.service';
import { MenuService } from '../../provider/menu.service';
import { jsonpFactory } from '@angular/http/src/http_module';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  produtos: any;
  nome: any;
  loading: any;
  detalhe: NavigationExtras;

  produtoItem: Array<{ codigo: any, qtd: any,  nome: any, valor: any, descricao: any, status: any, foto: any, data_registro: any }>;
  produtoItemTodos: Array<{ codigo: any, qtd: any, nome: any, valor: any, descricao: any, status: any, foto: any, data_registro: any }>;


  constructor(public http: Http, public serviceUrl: UrlService, public nav: NavController, private route: Router, public serviceUser: ServiceUserService, public menuService: MenuService) {


    this.validarMenu();
    this.listProdutos();
    console.log(this.serviceUser.getUserNome());


  }


  validarMenu() {

    if (localStorage.getItem('user_logado') != null) {
      console.log("usuario logado");

      this.serviceUser.setUserId(localStorage.getItem('idUser'));
      this.serviceUser.setUserNivel(localStorage.getItem('nivelUser'));
      this.serviceUser.setUserNome(localStorage.getItem('NameUser'));
      this.serviceUser.setUserFoto(localStorage.getItem('setUserFoto'));


    } else {
      this.nav.navigateBack('login');
    }


    if (this.serviceUser.userNivel == "2") {
      this.menuService.perfilMenu.push(this.menuService.menu[0]); //pulicar produto
    }
    if (this.serviceUser.userNivel == "1") {
      this.menuService.perfilMenu.push(this.menuService.menu[0]); //publicar produto
      this.menuService.perfilMenu.push(this.menuService.menu[1]); // cadastrar usuario
      this.menuService.perfilMenu.push(this.menuService.menu[2]); // cadastrar empresa
    }

  }

  listProdutos() {
    this.produtoItem = [];
    this.serviceUrl.exibirLoading();

    this.http.get(this.serviceUrl.getUrl() + "listDados.php").pipe(map(res => res.json()))
      .subscribe(
        listDados => {
          this.produtos = listDados;

          for (let i = 0; i < listDados.length; i++) {
            this.produtoItem.push({
              codigo: listDados[i]["codigo"],
              nome: listDados[i]["nome"],
              valor: listDados[i]["valor"],
              foto: listDados[i]["foto"],
              qtd: listDados[i]["quantidade"],
              descricao: listDados[i]["descricao"],
              status: listDados[i]["status"],
              data_registro: listDados[i]["criacao"]
            });
          }


          this.produtoItemTodos = this.produtoItem;

        }, error => {
          this.serviceUrl.alertas('Atenção', 'não possível carregar os produtos, verifique sua conexão!');
          this.serviceUrl.fecharLoding();

        }
      );
    this.serviceUrl.fecharLoding();

  }

  toProdutos() {
    this.nav.navigateForward('/cadastro_produtos');
  }




  go(id) {
    console.log(id);
    this.route.navigateByUrl('/list_produtos/id');
  }

  getItems(ev: any) {
    // Reset items back to all of the items


    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.produtoItem = this.produtoItemTodos.filter((produto) => {
        return (produto.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    } else {
      this.produtoItem = this.produtoItemTodos;
    }
    console.log(this.produtoItem);
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.listProdutos();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }



delete(codigo){
  let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    return this.http.post(this.serviceUrl.url + "deleteProduto.php", codigo, {
      headers: headers,
      method: "POST"
    }).pipe(map(
      (res: Response) => { return res; }
    ));
}

deleteProduto(produto){
  this.delete(produto.codigo)
  .subscribe(
    data =>{
      console.log('produto deletado com sucesso');
      this.listProdutos();
    }, error =>{
      console.log("erro ao tentar excluir"+ error)
    }
  );
}

editProduto(produto){
  this.detalhe = {
    queryParams: {
      "codigo": produto.codigo,
      "nome": produto.nome,
      "valor": produto.valor,
      "descricao": produto.descricao,
      "quantidade": produto.qtd
    }
  };

  this.route.navigate(['cadastro_produtos'], this.detalhe);
}

 

}
