import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-introducao',
  templateUrl: './introducao.page.html',
  styleUrls: ['./introducao.page.scss'],
})
export class IntroducaoPage implements OnInit {
  flag = false;

  constructor(public nav: NavController) { 
    if(this.flag!=true){
      if(localStorage.getItem('intro')=='sim'){
        this.nav.navigateForward('login');
      }
    }
   }

  ngOnInit() {
  }


  toIntro(){
    localStorage.setItem('intro', 'sim');
    this.nav.navigateForward('login');
  }

}
