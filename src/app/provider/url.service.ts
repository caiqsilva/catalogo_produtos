import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { analyzeAndValidateNgModules } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class UrlService {

  //url: string = "http://glidersi-esy-es.umbler.net/php/";
    url: string = "http://localhost/catalago_produtos/php/";

    loading = false;
    
  constructor(public alert: AlertController, public loadingCrtl: LoadingController) { 
    
  }

  getUrl(){
    return this.url;
  }


  async alertas(titulo, msg){
    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: [
        'OK'
      ]
    });
    await alert.present();
  }

  async exibirLoading(){
    this.loading = true
    return await this.loadingCrtl.create({
      message: 'Lintando dados...'
    }).then(a =>{
      a.present().then(() =>{
        if(!this.loading){
          a.dismiss().then(() => {});
        }
      });
    });
  }

  async fecharLoding(){
    this.loading = false;
    return await this.loadingCrtl.dismiss().then(() => {});
  }


 

}
